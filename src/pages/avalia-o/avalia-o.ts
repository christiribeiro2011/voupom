import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { AvaliaEsPage } from '../avalia-es/avalia-es';
@IonicPage()
@Component({
  selector: 'page-avalia-o',
  templateUrl: 'avalia-o.html'
})
export class AvaliaOPage {

  constructor(public navCtrl: NavController) {
  }
  goToAvaliaEs(params){
    if (!params) params = {};
    this.navCtrl.push(AvaliaEsPage);
  }
}
