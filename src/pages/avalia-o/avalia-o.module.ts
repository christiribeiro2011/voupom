import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AvaliaOPage } from "./avalia-o";

@NgModule({
  declarations: [
    AvaliaOPage
  ],
  imports: [
    IonicPageModule.forChild(AvaliaOPage)
  ]
})

export class AvaliaOPageModule { }
