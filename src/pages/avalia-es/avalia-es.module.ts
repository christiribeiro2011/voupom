import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AvaliaEsPage } from "./avalia-es";

@NgModule({
  declarations: [
    AvaliaEsPage
  ],
  imports: [
    IonicPageModule.forChild(AvaliaEsPage)
  ]
})

export class AvaliaEsPageModule { }
