import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-avalia-es',
  templateUrl: 'avalia-es.html'
})
export class AvaliaEsPage {

  constructor(public navCtrl: NavController) {
  }

}
