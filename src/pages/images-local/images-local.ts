import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ActionSheetController, ToastController, Platform, LoadingController, normalizeURL } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import firebase from 'firebase/app';

import { AngularFireStorage} from "angularfire2/storage";

@IonicPage()
@Component({
  selector: 'page-images-local',
  templateUrl: 'images-local.html',
})
export class ImagesLocalPage {
  public myPhotosRef: any;
  public myPhoto: any;
  public myPhotoURL: any;
  uid: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public platform: Platform,
    public imagePicker: ImagePicker,
    public cropService: Crop,
    public angualarFireStorage : AngularFireStorage,
    public loadingCtrl: LoadingController) {
    this.myPhotosRef = this.angualarFireStorage.ref("images");
    console.log(this.myPhotosRef);

    this.uid = navParams.get('uid');

  }

  uploadImage(imageURI) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child('imageName');
      this.encodeImageUri(imageURI, function (image64) {
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            resolve(snapshot.downloadURL)
          }, err => {
            reject(err);
          })
      })
    })
  }


  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  };


  openImagePicker() {
    this.imagePicker.hasReadPermission().then(
      (result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        }
        else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.uploadImageToFirebase(results[i]);

              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }


  uploadImageToFirebase(image) {
    image = normalizeURL(image);
    //uploads img to firebase storage
    this.myPhotosRef.uploadImage(image)
      .then(photoURL => {

        let toast = this.toastCtrl.create({
          message: 'Image was updated successfully',
          duration: 3000
        });
        toast.present();
        this.navCtrl.push('ImagesLocalPage', {
          uid: this.uid
        });
      })
  }


  openImagePickerCrop() {
    this.imagePicker.hasReadPermission().then(
      (result) => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        }
        else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.cropService.crop(results[i], { quality: 75 }).then(
                  newImage => {
                    this.uploadImageToFirebase(newImage);
                  },
                  error => console.error("Error cropping image", error)
                );
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  ionViewDidLoad() {
    console.log(this.myPhotosRef);

    this.viewCtrl.setBackButtonText('');
    console.log('ionViewDidLoad ImagesLocalPage');
  }


  dimiss() {
    this.viewCtrl.dismiss();
  }





  gotoProximo(params) {
    if (!params) params = {};
    this.navCtrl.push('RestaurantesPage');
  }





}
