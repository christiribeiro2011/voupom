import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImagesLocalPage } from './images-local';



@NgModule({
  declarations: [
    ImagesLocalPage,
  ],
  imports: [
    IonicPageModule.forChild(ImagesLocalPage),

  ],
})
export class ImagesLocalPageModule {}
