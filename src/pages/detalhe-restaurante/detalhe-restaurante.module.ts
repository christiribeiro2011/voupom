import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { DetalheRestaurantePage } from "./detalhe-restaurante";

@NgModule({
  declarations: [
    DetalheRestaurantePage
  ],
  imports: [
    IonicPageModule.forChild(DetalheRestaurantePage)
  ]
})

export class DetalheRestaurantePageModule { }
