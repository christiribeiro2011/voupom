import { Component } from "@angular/core";
import {
  NavController,
  IonicPage,
  ViewController,
  NavParams
} from "ionic-angular";
import { ImageViewerController } from "ionic-img-viewer";
import { Geolocation } from "@ionic-native/geolocation";
import { EstabelecimentoProvider } from "../../providers/estabelecimento/estabelecimento";
import { AngularFireList, AngularFireDatabase } from "angularfire2/database";
import { Observable } from "rxjs-compat/Observable";
import { map } from "rxjs/operators";
import { Endereco } from "../../models/endereco/endereco.model";

declare var google;

@IonicPage()
@Component({
  selector: "page-detalhe-restaurante",
  templateUrl: "detalhe-restaurante.html"
})
export class DetalheRestaurantePage {

  endereco: Endereco;
  mapa: string;


  map: any;

  _imageViewerCtrl: ImageViewerController;
  idEstabelecimento: any;
  itemsRef: AngularFireList<any>;
  estabelecimento: Observable<any>;
  detalheRestaurante: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public imageViewerCtrl: ImageViewerController,
    private geolocation: Geolocation,
    public provider: EstabelecimentoProvider,
    public db: AngularFireDatabase
  ) {


    this._imageViewerCtrl = imageViewerCtrl;

    if (this.navParams.get("params")) {
      this.detalheRestaurante = this.navParams.get("params");
      console.log(this.detalheRestaurante);
      let id = this.detalheRestaurante.uid;

      this.itemsRef = db.list("estabelecimentos/" + id);
      this.estabelecimento = this.itemsRef.snapshotChanges().pipe(
        map(
          changes => console.log(changes)
          // changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      );
    }
    console.log(this.getEndereco());

    this.mapa = this.getMapa();
  }

  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText("");

    const position = new google.maps.LatLng(this.detalheRestaurante.lat, this.detalheRestaurante.long);

    const mapOptions = {
      zoom: 18,
      center: position
      //disableDefaultUI: true
    };

    this.map = new google.maps.Map(document.getElementById("map"), mapOptions);

    const marker = new google.maps.Marker({
      position: position,
      map: this.map,

      // Titulo
      title: 'Minha posição',

      // Animção
      animation: google.maps.Animation.DROP, // BOUNCE

      // Icone
      icon: 'assets/img/placeholder.png'
    });
  }

  private getEndereco() {
    return this.detalheRestaurante.logradouro + ', ' + this.detalheRestaurante.numero + ' - ' + this.detalheRestaurante.bairro + ', ' + this.detalheRestaurante.cidade + ' - ' + this.detalheRestaurante.estado;
  }

  private getMapa() {


    return 'https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=400x400&markers=color:red|' + this.getEndereco() + '&key=AIzaSyCxqgKqZMHNzOV2TETOwjRJAUpuh3aeK1c'
  }

  goToCupom(params) {
    if (!params) params = {};
    this.navCtrl.push("CupomPage");
  }
  goToAvaliaO(params) {
    if (!params) params = {};
    this.navCtrl.push("AvaliaOPage");
  }
  goToAvaliaEs(params) {
    if (!params) params = {};
    this.navCtrl.push("AvaliaEsPage");
  }

  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }
}
