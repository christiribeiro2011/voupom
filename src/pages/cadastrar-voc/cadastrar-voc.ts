import { Component } from '@angular/core';
import { NavController, IonicPage, ViewController, ToastController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from "@angular/forms";
import { UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';
import { User } from '../../models/user/user.model';
import { FirebaseAuth } from 'angularfire2';
import firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-cadastrar-voc',
  templateUrl: 'cadastrar-voc.html'
})
export class CadastrarVocPage {

  signupForm: FormGroup;
  nome: AbstractControl;
  sobrenome: AbstractControl;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private provider: UserProvider,
    private authProvider: AuthProvider
  ) {

    let emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    this.signupForm = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(3)]],
      sobrenome: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, { validator: this.checkIfMatchingPasswords('password', 'confirmPassword') });


    }

  onSubmit() {
    if (this.signupForm.valid) {

      let formUser = this.signupForm.value;
      this.authProvider.createAuthUser({
        email: formUser.email,
        password: formUser.password
      }).then((authUser: firebase.User) => {

        delete formUser.senha;
        formUser.uid = authUser.uid;

        console.log(formUser.uid);

        this.provider.save(formUser, formUser.uid)
          .then(() => {
            this.toast.create({ message: 'Contato salvo com sucesso.', duration: 3000 }).present();
            this.navCtrl.pop();
          })
          .catch((e) => {
            this.toast.create({ message: 'Erro ao salvar o contato.', duration: 3000 }).present();
            console.error(e);
          })

      })
      .catch((e) => {
        this.toast.create({ message: 'Erro ao salvar o contato.', duration: 3000 }).present();
        console.error(e);
      });

    }
  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText('');
  }

  goToCadastroEstabelecimento(params) {

    if (!params) params = {};
    this.navCtrl.push('CadastrarEstabelecimentoPage');
  }


  dimiss() {
    this.viewCtrl.dismiss();
  }
}
