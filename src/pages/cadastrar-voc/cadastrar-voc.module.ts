import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CadastrarVocPage } from "./cadastrar-voc";
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    CadastrarVocPage
  ],
  imports: [
    BrMaskerModule,
    IonicPageModule.forChild(CadastrarVocPage)
  ]
})

export class CadastrarVocPageModule { }
