import { Component } from '@angular/core';
import { NavController, IonicPage, ViewController, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, FormGroup, Validators, AbstractControl } from "@angular/forms";
import { FirebaseAuth } from 'angularfire2';
import firebase from 'firebase/app';
import { Facebook } from '@ionic-native/facebook'


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  loginForm: FormGroup;

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public firebaseauth: AngularFireAuth,
    public formBuilder: FormBuilder,
    public facebook: Facebook) {

    let emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
      password: ['', Validators.required]
    });


  }
  goToRestaurantes(params) {
    if (!params) params = {};
    this.navCtrl.push('RestaurantesPage');
  } goToDetalheRestaurante(params) {
    if (!params) params = {};
    this.navCtrl.push('DetalheRestaurantePage');
  } goToCupom(params) {
    if (!params) params = {};
    this.navCtrl.push('CupomPage');
  } goToAvaliaO(params) {
    if (!params) params = {};
    this.navCtrl.push('AvaliaOPage');
  } goToAvaliaEs(params) {
    if (!params) params = {};
    this.navCtrl.push('AvaliaEsPage');
  } goToCadastrarVoc(params) {
    if (!params) params = {};
    this.navCtrl.push('CadastrarVocPage');
  } goToCadastrarRestaurante(params) {
    if (!params) params = {};
    this.navCtrl.push('CadastrarRestaurantePage');
  } goToEsqueciSenha(params) {
    if (!params) params = {};
    this.navCtrl.push('ResetePasswordPage');
  }

  dimiss() {
    this.viewCtrl.dismiss();
  }

  public LoginComEmail(): void {
    let formUser = this.loginForm.value;
    this.firebaseauth.auth.signInWithEmailAndPassword(formUser.email, formUser.password)
      .then(() => {
        this.toastCtrl.create({ message: 'Logado com sucesso.', duration: 3000 }).present();
        this.navCtrl.setRoot("RestaurantesPage")
      })
      .catch((erro: any) => {
        this.toastCtrl.create({ message: 'erro' + erro, duration: 3000 }).present();
      });
  }

  public async facebookLogin(): Promise<any> {
    try {
      const response = await this.facebook.login(['email']);
      const facebookCredential = firebase.auth.FacebookAuthProvider
        .credential(response.authResponse.accessToken);
      firebase.auth().signInWithCredential(facebookCredential)
        .then(success => {
          console.log("Firebase success: " + JSON.stringify(success));
        });
    }
    catch (error) {
      console.log(error);
    }
  }


}
