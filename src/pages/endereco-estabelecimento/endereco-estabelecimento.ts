import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from "@angular/forms";
import { Endereco } from '../../models/endereco/endereco.model';
import { FirebaseAuth } from 'angularfire2';
import firebase from 'firebase/app';
import { EstabelecimentoProvider } from '../../providers/estabelecimento/estabelecimento';
import { AuthProvider } from '../../providers/auth/auth';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Geolocation } from "@ionic-native/geolocation";


@IonicPage()
@Component({
  selector: 'page-endereco-estabelecimento',
  templateUrl: 'endereco-estabelecimento.html',
})
export class EnderecoEstabelecimentoPage {
  addressForm: FormGroup;
  uid: any;
  private endereco: Endereco;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private provider: EstabelecimentoProvider,
    private authProvider: AuthProvider,
    private geolocation: Geolocation) {


    this.addressForm = this.formBuilder.group({
      logradouro: ['', [Validators.required, Validators.minLength(1)]],
      numero: ['', [Validators.required, Validators.minLength(1)]],
      bairro: ['', [Validators.required, Validators.minLength(1)]],
      cep: ['', [Validators.required, Validators.minLength(1)]],
      cidade: ['', [Validators.required, Validators.minLength(1)]],
      estado: ['', [Validators.required, Validators.minLength(1)]],
      complemento: ['', [Validators.required, Validators.minLength(1)]],
      lat: [],
      long: []
    });

    this.uid = navParams.get('uid');

    console.log(this.uid)

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EnderecoEstabelecimentoPage');
    this.viewCtrl.setBackButtonText('');
  }


  saveAddress() {
    if (this.addressForm.valid) {

      console.log(this.uid);
      this.geolocation.getCurrentPosition().then((resp) => {
        this.addressForm.value.lat = resp.coords.latitude
        this.addressForm.value.long = resp.coords.longitude

        this.provider.enderecoLocal(this.addressForm.value, this.uid)
          .then(() => {

            this.toast.create({ message: 'Endereço salvo com sucesso.', duration: 3000 }).present();
            this.navCtrl.push('RestaurantesPage', {
              uid: this.uid
            });

          })
          .catch((e) => {
            this.toast.create({ message: 'Erro ao salvar o Endereço.', duration: 3000 }).present();
            console.error(e);
          })
      }).catch((error) => {
        console.log('Error getting location', error);
      });



  }
}


dimiss() {
  this.viewCtrl.dismiss();
}

gotoProximo(params) {
  if (!params) params = {};
  this.navCtrl.push('MapPage');
}


}
