import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EnderecoEstabelecimentoPage } from './endereco-estabelecimento';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    EnderecoEstabelecimentoPage,
  ],
  imports: [
    BrMaskerModule,
    IonicPageModule.forChild(EnderecoEstabelecimentoPage),
  ],
})
export class EnderecoEstabelecimentoPageModule {}
