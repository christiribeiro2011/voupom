import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResetePasswordPage } from './resete-password';

@NgModule({
  declarations: [
    ResetePasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(ResetePasswordPage),
  ],
})
export class ResetePasswordPageModule {}
