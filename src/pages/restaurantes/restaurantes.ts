import { Component, ViewChild, ElementRef } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  Searchbar
} from "ionic-angular";
import { Observable } from "rxjs-compat/Observable";
import { EstabelecimentoProvider } from "../../providers/estabelecimento/estabelecimento";
import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { map } from "rxjs/operators";
import { DetalheRestaurantePage } from "../detalhe-restaurante/detalhe-restaurante";

/**
 * Generated class for the RestaurantesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-restaurantes",
  templateUrl: "restaurantes.html"
})
export class RestaurantesPage {
  @ViewChild("searchbar", { read: ElementRef }) searchbarRef: ElementRef;
  @ViewChild("searchbar") searchbarElement: Searchbar;
  search: boolean = false;
  queryText: string;
  itemsRef: AngularFireList<any>;
  estabelecimentos: Observable<any>;
  estabelecimentoLocal: Observable<any>;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public provider: EstabelecimentoProvider,
    public db: AngularFireDatabase
  ) {
    this.itemsRef = db.list("estabelecimentos/");
    this.estabelecimentos = this.itemsRef
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      );
  }

  goToDetalheRestaurante(params) {
    if (!params) params = {};
    this.navCtrl.push(DetalheRestaurantePage, { params: params });
  }

  ionViewDidLoad() {
    console.log(this.estabelecimentos);
    this.viewCtrl.setBackButtonText("");
    console.log("ionViewDidLoad RestaurantesPage");
  }

  toggleSearch() {
    if (this.search) {
      this.search = false;
    } else {
      this.search = true;
      this.searchbarElement.setFocus();
    }
  }

  searchAction(texto: any) {
    let val = texto.target.value;
    //implement search
  }
}
