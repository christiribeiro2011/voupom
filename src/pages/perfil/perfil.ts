import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  options: BarcodeScannerOptions;
  encodeText: string = '';
  encodeData: any = {};
  scannerData: any = {};

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public scanner: BarcodeScanner) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  scan() {
    this.options = {
      prompt: 'Scan you barcode'
    };

    this.scanner.scan(this.options).then((data)=>{
      this.scannerData = data;
    }, (err)=> {
      console.log(err);
    });
  }

  encode() {
    this.scanner.encode(this.scanner.Encode.TEXT_TYPE, this.encodeText ).then((data) => {
      this.encodeData = data;
    }, (err) => {
      console.log(err);
     })
  }

}
