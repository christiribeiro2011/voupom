import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastrarEstabelecimentoPage } from './cadastrar-estabelecimento';
import { BrMaskerModule } from 'brmasker-ionic-3';


@NgModule({
  declarations: [
    CadastrarEstabelecimentoPage,
  ],
  imports: [
    BrMaskerModule,
    IonicPageModule.forChild(CadastrarEstabelecimentoPage),
  ],
})
export class CadastrarEstabelecimentoPageModule {}
