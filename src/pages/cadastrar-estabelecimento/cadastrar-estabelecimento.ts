import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from "@angular/forms";
// import { Estabelecimento } from '../../models/estabelecimento/estabelecimento.model';
// import { FirebaseAuth } from 'angularfire2';
import firebase from 'firebase/app';
import { EstabelecimentoProvider } from '../../providers/estabelecimento/estabelecimento';
import { AuthProvider } from '../../providers/auth/auth';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';


@IonicPage()
@Component({
  selector: 'page-cadastrar-estabelecimento',
  templateUrl: 'cadastrar-estabelecimento.html',
})
export class CadastrarEstabelecimentoPage {
  public base64Image: string;
  private filePhoto: File;

  urlImg: any;

  picdata: any;
  mypicref: any;

  signupForm: FormGroup;

  imgPath: string;
  fileToUpload: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    private camera: Camera,
    private toast: ToastController,
    private provider: EstabelecimentoProvider,
    private authProvider: AuthProvider,
    private imagePicker: ImagePicker,
    ) {

    this.mypicref = firebase.storage().ref('estabelecimentos/');

    let emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    this.signupForm = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(1)]],
      descricao: ['', [Validators.required, Validators.minLength(1)]],
      horario: ['', [Validators.required, Validators.minLength(1)]],
      telefone: ['', [Validators.required, Validators.minLength(1)]],
      cnpj: ['', [Validators.required, Validators.minLength(1)]],

      email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    }, { validator: this.checkIfMatchingPasswords('password', 'confirmPassword') });

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastrarEstabelecimentoPage');
    this.viewCtrl.setBackButtonText('');
  }

  onSubmit() {
    if (this.signupForm.valid) {

      let formUser = this.signupForm.value;
      this.authProvider.createAuthUser({
        email: formUser.email,
        password: formUser.password
      }).then((authUser: firebase.User) => {

        delete formUser.senha;
        formUser.uid = authUser.uid;




        // this.uploadPhoto(formUser.uid);
        // formUser.image = this.base64Image;
        // console.log(formUser);


        this.provider.create(formUser, formUser.uid)
          .then((data) => {
            console.log(data);

            this.toast.create({ message: 'Estabelecimento salvo com sucesso.', duration: 3000 }).present();
            this.navCtrl.push('EnderecoEstabelecimentoPage', {
              uid: formUser.uid
            });

          })
          .catch((e) => {
            this.toast.create({ message: 'Erro ao salvar o Estabelecimento.', duration: 3000 }).present();
            console.error(e);
          })

      })
        .catch((e) => {
          this.toast.create({ message: 'Erro ao salvar o Estabelecimento.', duration: 3000 }).present();
          console.error(e);
        });

    }
  }

  openPhoto(event): void {
    console.log(event.target.files);
    this.base64Image = event.target.files[0];
  }

  openCamera() {
    this.camera.getPicture({
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.PNG,
      saveToPhotoAlbum: true,
      targetWidth: 1000,
      targetHeight: 1000
    }).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = imageData;
      this.signupForm ;
    }, (err) => {
      console.log(err);
    })
  }


  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  }


  upload(userId) {
    console.log(this.base64Image);
    this.encodeImageUri(this.base64Image, function (image64) {
      firebase.storage()
        .ref()
        .child(`/estabelecimento/${userId}`)
        .putString(this.base64Image, `base64`, { contentType: `image/png` })
        .then(savepic => {
          this.base64Image = savepic.downloadURL
        })
    })
  }
  uploadImage(userId) {
    return new Promise<any>((resolve, reject) => {

      this.encodeImageUri(this.base64Image, function (image64) {
        firebase.storage()
          .ref()
          .child(`/estabelecimento/${userId}`)
          .putString(this.base64Image, `base64`, { contentType: `image/png` })
          .then(savepic => {
            this.base64Image = savepic.downloadURL
          })
      })
    })
  }


  openGaleria() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      console.log(imageData, this.base64Image);
      this.base64Image = imageData;

    }, (err) => {
      console.log(err);
      this.toast.create({ message: 'Erro ao salvar o imagem.', duration: 3000 }).present();
    });
  }


  dimiss() {
    this.viewCtrl.dismiss();
  }

  gotoProximo(params) {
    if (!params) params = {};
    this.navCtrl.push('MapPage');
  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }



  selectPhoto(): void {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality: 100,
      encodingType: this.camera.EncodingType.PNG,
    }).then(imageData => {
      this.base64Image = imageData;

    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }

  private uploadPhoto(uid): void {
    let myPhotosRef = firebase.storage().ref('/estabelecimentos/');
    myPhotosRef.child(this.generateUUID()).child('myPhoto.png')
      .putString(this.base64Image, 'base64', { contentType: 'image/png' })
      .then((savedPicture) => {
        this.base64Image = savedPicture.downloadURL;
      });
  }

  private generateUUID(): any {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }



}
