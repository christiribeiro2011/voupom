import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  goToLogin(){
    let loginModal = this.modalCtrl.create('LoginPage');
    loginModal.present();
  }

  goToCadastro(){
    this.navCtrl.push('CadastrarVocPage');
    // let cadastroModal = this.modalCtrl.create('CadastrarRestaurantePage');
    // cadastroModal.present();
  }

}
