import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cupom',
  templateUrl: 'cupom.html'
})
export class CupomPage {

  constructor(public navCtrl: NavController) {
  }

}
