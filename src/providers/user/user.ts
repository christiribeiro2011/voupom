import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../models/user/user.model';
import { map } from 'rxjs/operators';


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  private PATH = 'users/';

  constructor(private db: AngularFireDatabase, private auth: AngularFireAuth) {
    console.log('Hello UserProvider Provider');
  }

  getAll() {
    return this.db.list(this.PATH, ref => ref.orderByChild('name'))
      .snapshotChanges()
      .pipe(
        map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      }))
  }

  get(key: string) {
    return this.db.object(this.PATH + key).snapshotChanges()
      .pipe(
        map(c => {
        return { key: c.key, ...c.payload.val() };
      }));
  }
  create(user: User, uuid: string): Promise<void> {
    return this.db.object(`/users/${uuid}`)
      .set(user);
  }

  save(user: any, uuid: string) {
    return new Promise((resolve, reject) => {
      if (user.key) {
        this.db.list(this.PATH + uuid)
          .set(user.key, { nome: user.nome, sobrenome: user.sobrenome, email: user.email, uid: user.uid })
          .then(() => resolve())
          .catch((e) => reject(e));
      } else {
        this.db.list(this.PATH)
          .push({ nome: user.nome, sobrenome: user.sobrenome, email: user.email, uid: user.uid })
          .then(() => resolve());
      }
    })
  }

  remove(key: string) {
    return this.db.list(this.PATH).remove(key);
  }

}
