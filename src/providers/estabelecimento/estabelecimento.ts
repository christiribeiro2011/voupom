import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Estabelecimento } from '../../models/estabelecimento/estabelecimento.model';
import { Endereco } from '../../models/endereco/endereco.model';

import firebase from 'firebase/app';
import { normalizeURL } from 'ionic-angular';
import { map } from 'rxjs/operators';


@Injectable()
export class EstabelecimentoProvider {

  private PATH = 'estabelecimentos/';

  constructor(private db: AngularFireDatabase, private auth: AngularFireAuth) {
    console.log('Hello EstabelecimentoProvider Provider');

  }

  getAll() {
    return this.db.list(this.PATH, ref => ref.orderByChild('name'))
      .snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        }))

  }

  get(key: string) {
    return this.db.object(this.PATH + key).snapshotChanges()
      .pipe(
        map(c => {
          return { key: c.key, ...c.payload.val() };
        }));


  }
  create(user: Estabelecimento, uuid: string): Promise<void> {
    return this.db.object(this.PATH + uuid)
      .set(user);
  }


  save(user: Estabelecimento, uuid: string) {
    return new Promise((resolve, reject) => {
      if (user.uid) {
        this.db.list(this.PATH + uuid)
          .update(user.uid, { nome: user.nome, descricao: user.descricao, horario: user.horario, telefone: user.telefone, cnpj: user.cnpj, email: user.email, lat: user.lat, log: user.long, uid: user.uid })
          .then(() => resolve())
          .catch((e) => reject(e));
      } else {
        this.db.list(this.PATH)
          .push({ nome: user.nome, descricao: user.descricao, horario: user.horario, telefone: user.telefone, cnpj: user.cnpj, email: user.email, uid: user.uid })
          .then(() => resolve());
      }
    })
  }

  saveMap(lat: any, log: any, uuid: string) {
    return new Promise((resolve, reject) => {
      if (uuid) {
        // this.db.list(this.PATH + uuid)
        //   .update(uuid, { lat: lat, log: log })

        this.db.object(this.PATH + uuid)
          .update({ lat: lat, long: log })
          .then(() => resolve())
          .catch((e) => reject(e));
      }
    })
  }

  enderecoLocal(item: Endereco, uid: any) {
    return new Promise((resolve, reject) => {
      if (uid) {
        // this.db.list(this.PATH + uuid)
        //   .update(uuid, { lat: lat, log: log })

        this.db.object(this.PATH + uid)
          .update({
            cep: item.cep,
            logradouro: item.logradouro,
            numero: item.numero,
            bairro: item.bairro,
            cidade: item.cidade,
            estado: item.estado,
            complemento: item.complemento,
            long: item.long,
            lat: item.lat
          })
          .then(() => resolve())
          .catch((e) => reject(e));
      } else {
        this.db.list(this.PATH)
          .push({
            logradouro: item.logradouro,
            numero: item.numero,
            bairro: item.bairro,
            cidade: item.cidade,
            estado: item.estado,
            complemento: item.complemento,
            long: item.long,
            lat: item.lat
           })
          .then(() => resolve());
      }
    })
  }


  uploadImage(file: File, userId: string): firebase.storage.UploadTask {


    return firebase.storage()
      .ref()
      .child(`/estabelecimento/${userId}`)
      .put(file);
  }

  remove(key: string) {
    return this.db.list(this.PATH).remove(key);
  }



}
