import { Injectable } from '@angular/core';

import { User } from './../../models/user/user.model';

import { AngularFireAuth } from "angularfire2/auth";
import firebase from 'firebase/app';
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  private userF: any;
  constructor(public auth: AngularFireAuth) {
    console.log('Hello AuthProvider Provider');
    auth.authState.subscribe(user => {
      this.userF = user;
    });
  }

  get authenticated(): boolean {
    return this.userF !== null;
  }

  getEmail() {
    return this.userF && this.userF.email;
  }


  createAuthUser(user: { email: string, password: string }): Promise<any> {
    return firebase
      .auth()
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(function (user) {
        return firebase.auth().currentUser
      }).catch(error => {
        console.error(error);
        throw new Error(error);
      });


    }

  signOut(): Promise<void> {
    return this.auth.auth.signOut();
  }

  // logout() {
  //   this.storage.set("token", "");
  //   this.storage.set("isLogged", false);
  //   this.storage.set("email", "");
  // }

  // setUser(user: { email: string, password: string }) {
  //   this.storage.set("token", "dG9rZW5zZWNyZXRv");
  //   this.storage.set("isLogged", true);
  //   this.storage.set("email", user.email);
  // }



}
