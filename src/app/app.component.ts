import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav : Nav;
  pages: Array<{title: string, component: string, openTab?:any}>;

  rootPage: any = 'HomePage';

  private menu: MenuController;
  platform: any;
  statusBar: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private auth: AuthProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.initializeApp();

    this.pages = [
      { title: 'Locais', component: 'RestaurantesPage' },
      { title: 'Cupons', component: 'CupomPage' },
      { title: 'Perfil', component: 'PerfilPage' }
    ]
  }

  openPage(page) {
    this.nav.setRoot(page.component, { openTab: page.openTab })
  }

  initializeApp() {
    // this.platform.ready().then(() => {
    //   this.statusBar.styleDefault();
    // });

    this.auth.auth.authState
      .subscribe(
        user => {
          if (user) {
            this.rootPage = 'RestaurantesPage';
          } else {
            this.rootPage = 'HomePage';
          }
        },
        () => {
          this.rootPage = 'HomePage';
        }
      );
  }

  logout() {

    this.auth.signOut();
    this.nav.setRoot("HomePage");
  }



}
