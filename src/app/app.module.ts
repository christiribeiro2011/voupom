import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { MyApp } from "./app.component";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Camera } from "@ionic-native/camera";
import { Facebook } from "@ionic-native/facebook";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFireAuthModule } from "angularfire2/auth";
import { AngularFireStorage } from "angularfire2/storage";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { IonicImageViewerModule } from "ionic-img-viewer";

import { Geolocation } from "@ionic-native/geolocation";
import { LoginProvider } from "../providers/login/login";
import { UserProvider } from "../providers/user/user";
import { AuthProvider } from "../providers/auth/auth";
import { EstabelecimentoProvider } from "../providers/estabelecimento/estabelecimento";
import { Transfer } from "@ionic-native/transfer";
import { FilePath } from "@ionic-native/file-path";
import { ImagePicker } from "@ionic-native/image-picker";
import { Crop } from "@ionic-native/crop";
import { DetalheRestaurantePageModule } from "../pages/detalhe-restaurante/detalhe-restaurante.module";

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    BrowserAnimationsModule,
    IonicImageViewerModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyDF463WCBoLryMau2MuWxKh6O9TiDy_QFk",
      authDomain: "feedlize-9c26b.firebaseapp.com",
      databaseURL: "https://feedlize-9c26b.firebaseio.com",
      projectId: "feedlize-9c26b",
      storageBucket: "feedlize-9c26b.appspot.com",
      messagingSenderId: "501807117969"
    }),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    DetalheRestaurantePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Geolocation,
    Transfer,
    FilePath,
    Camera,
    ImagePicker,
    Crop,
    LoginProvider,
    UserProvider,
    AuthProvider,
    EstabelecimentoProvider,
    AngularFireStorage,
    Facebook,
    BarcodeScanner
  ]
})
export class AppModule {}
