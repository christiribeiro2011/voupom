export class Endereco {
  constructor(
    public logradouro: string,
    public numero: string,
    public bairro: string,
    public cidade: string,
    public estado: string,
    public complemento: string,
    public cep: string,
    public long: string,
    public lat: string,
    public uid: string,
  ) { }
}
