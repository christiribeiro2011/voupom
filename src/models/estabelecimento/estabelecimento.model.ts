export class Estabelecimento {
  constructor(
    public nome: string,
    public descricao: string,
    public horario: string,
    public telefone: string,
    public cnpj: string,
    public email: string,
    public uid: string,
    public long: number,
    public lat: number
  ) { }
}
